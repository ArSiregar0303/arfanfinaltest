package com.example.arfanfinaltest

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import com.example.arfanfinaltest.viewmodel.UserViewModel
import kotlinx.android.synthetic.main.activity_main.*

private var userViewModel: UserViewModel? = null

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        userViewModel = ViewModelProviders.of(this, UserViewModel.Factory(applicationContext)).get(UserViewModel::class.java)

        val signUp = getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE)
            .getBoolean("signup", true)

        if (signUp) {
            signup!!.visibility = View.VISIBLE
            login!!.visibility = View.GONE
        } else {
            signup!!.visibility = View.GONE
            login!!.visibility = View.VISIBLE
        }
        signup!!.setOnClickListener {
            userViewModel!!.createUser(username!!.text.toString(), password!!.text.toString())
            getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE).edit()
                .putBoolean("signup", false).commit()
            Toast.makeText(baseContext, "Successfully Created An Account!", Toast.LENGTH_LONG).show()
        }

        login!!.setOnClickListener {
            val isValid = userViewModel!!.checkValidLogin(username!!.text.toString(), password!!.text.toString())
            if (isValid) {
                Toast.makeText(baseContext, "Successfully Logged In!", Toast.LENGTH_LONG).show()
                Log.i("Successful_Login", "Login was successful")
            } else {
                Toast.makeText(baseContext, "Invalid Login!", Toast.LENGTH_SHORT).show()
                Log.i("Unsuccessful_Login", "Login was not successful")
            }
        }

    }
}